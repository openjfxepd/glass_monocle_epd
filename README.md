# Native Monocle EPD

This project is a dynamic library containing the native C implementation of a Monocle platform called “EPD” for devices with an electrophoretic display.

## Project

The NetBeans project uses a tool collection called “GNU_armhf” that defines the C compiler as `/usr/bin/arm-linux-gnueabihf-gcc`.
In Ubuntu 14.04.5 LTS, this compiler is contained in the package `gcc-arm-linux-gnueabihf` and can be installed as a dependency of the `crossbuild-essential-armhf` package with the command:

```shell_session
$ sudo apt-get install crossbuild-essential-armhf
```

The project requires two include directories for the C compiler:

```
${JAVA_HOME}/include
${JAVA_HOME}/include/linux
```

where `JAVA_HOME` is an environment variable set to the location of your JDK 8 installation.
These directories are required for the `jni.h` and `linux/jni_md.h` header files.

## Installation

Place the dynamic library on the target device in `$HOME/lib/arm/libglass_monocle_epd.so`.

Add the Java system property `-Djava.library.path=$HOME/lib/arm:$JAVA_HOME/jre/lib/arm:/lib:/usr/lib` to pick up the new dynamic library, where `JAVA_HOME` is set to the location of your JDK 8 installation with the OpenJFX overlay bundle.

Also install the Java implementation in [jfxepd](https://gitlab.com/openjfxepd/jfxepd).

