#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU_armhf-Linux
CND_ARTIFACT_DIR_Debug=dist/Debug/GNU_armhf-Linux
CND_ARTIFACT_NAME_Debug=libglass_monocle_epd.so
CND_ARTIFACT_PATH_Debug=dist/Debug/GNU_armhf-Linux/libglass_monocle_epd.so
CND_PACKAGE_DIR_Debug=dist/Debug/GNU_armhf-Linux/package
CND_PACKAGE_NAME_Debug=libglassmonocleepd.so.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU_armhf-Linux/package/libglassmonocleepd.so.tar
# Release configuration
CND_PLATFORM_Release=GNU_armhf-Linux
CND_ARTIFACT_DIR_Release=dist/Release/GNU_armhf-Linux
CND_ARTIFACT_NAME_Release=libglass_monocle_epd.so
CND_ARTIFACT_PATH_Release=dist/Release/GNU_armhf-Linux/libglass_monocle_epd.so
CND_PACKAGE_DIR_Release=dist/Release/GNU_armhf-Linux/package
CND_PACKAGE_NAME_Release=libglassmonocleepd.so.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU_armhf-Linux/package/libglassmonocleepd.so.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
