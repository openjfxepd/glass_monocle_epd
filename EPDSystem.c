/*
 * Copyright (c) 2016, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

// Native methods implementation of EPDSystem.java

#include <sys/ioctl.h>  // For ioctl
#include <sys/types.h>  // For uint
#include "mxcfb.h"

#include "EPDSystem.h"
#include "Monocle.h"

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_ioctl
(JNIEnv *UNUSED(env), jobject UNUSED(obj), jlong fd, jint request, jint value) {
    return ioctl((int) fd, (int) request, (__u32 *) & value);
}

// EPDSystem.IntegerStructure

struct integer_structure {
    __u32 integer;
};

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024IntegerStructure_sizeof
(JNIEnv * env, jobject object) {
    return (jint) sizeof (struct integer_structure);
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024IntegerStructure_getInteger
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct integer_structure *) asPtr(p))->integer;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024IntegerStructure_setInteger
(JNIEnv * env, jobject object, jlong p, jint integer) {
    struct integer_structure *ptr = (struct integer_structure *) asPtr(p);
    ptr->integer = (__u32) integer;
}

// EPDSystem_FbVarScreenInfo

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getGrayscale
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->grayscale;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getRedOffset
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->red.offset;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getRedLength
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->red.length;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getRedMsbRight
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->red.msb_right;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getGreenOffset
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->green.offset;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getGreenLength
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->green.length;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getGreenMsbRight
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->green.msb_right;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getBlueOffset
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->blue.offset;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getBlueLength
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->blue.length;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getBlueMsbRight
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->blue.msb_right;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getTranspOffset
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->transp.offset;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getTranspLength
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->transp.length;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getTranspMsbRight
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->transp.msb_right;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getNonstd
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->nonstd;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getActivate
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->activate;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getHeight
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->height;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getWidth
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->width;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getAccelFlags
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->accel_flags;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getPixclock
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->pixclock;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getLeftMargin
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->left_margin;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getRightMargin
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->right_margin;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getUpperMargin
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->upper_margin;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getLowerMargin
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->lower_margin;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getHsyncLen
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->hsync_len;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getVsyncLen
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->vsync_len;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getSync
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->sync;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getVmode
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->vmode;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getRotate
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->rotate;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_getColorspace
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct fb_var_screeninfo *) asPtr(p))->colorspace;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_setGrayscale
(JNIEnv * env, jobject object, jlong p, jint grayscale) {
    struct fb_var_screeninfo *ptr = (struct fb_var_screeninfo *) asPtr(p);
    ptr->grayscale = grayscale;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_setNonstd
(JNIEnv * env, jobject object, jlong p, jint nonstd) {
    struct fb_var_screeninfo *ptr = (struct fb_var_screeninfo *) asPtr(p);
    ptr->nonstd = nonstd;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_setHeight
(JNIEnv * env, jobject object, jlong p, jint height) {
    struct fb_var_screeninfo *ptr = (struct fb_var_screeninfo *) asPtr(p);
    ptr->height = height;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_setWidth
(JNIEnv * env, jobject object, jlong p, jint width) {
    struct fb_var_screeninfo *ptr = (struct fb_var_screeninfo *) asPtr(p);
    ptr->width = width;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_setAccelFlags
(JNIEnv * env, jobject object, jlong p, jint accelFlags) {
    struct fb_var_screeninfo *ptr = (struct fb_var_screeninfo *) asPtr(p);
    ptr->accel_flags = accelFlags;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_setPixclock
(JNIEnv * env, jobject object, jlong p, jint pixclock) {
    struct fb_var_screeninfo *ptr = (struct fb_var_screeninfo *) asPtr(p);
    ptr->pixclock = pixclock;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_setLeftMargin
(JNIEnv * env, jobject object, jlong p, jint leftMargin) {
    struct fb_var_screeninfo *ptr = (struct fb_var_screeninfo *) asPtr(p);
    ptr->left_margin = leftMargin;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_setRightMargin
(JNIEnv * env, jobject object, jlong p, jint rightMargin) {
    struct fb_var_screeninfo *ptr = (struct fb_var_screeninfo *) asPtr(p);
    ptr->right_margin = rightMargin;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_setUpperMargin
(JNIEnv * env, jobject object, jlong p, jint upperMargin) {
    struct fb_var_screeninfo *ptr = (struct fb_var_screeninfo *) asPtr(p);
    ptr->upper_margin = upperMargin;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_setLowerMargin
(JNIEnv * env, jobject object, jlong p, jint lowerMargin) {
    struct fb_var_screeninfo *ptr = (struct fb_var_screeninfo *) asPtr(p);
    ptr->lower_margin = lowerMargin;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_setHsyncLen
(JNIEnv * env, jobject object, jlong p, jint hsyncLen) {
    struct fb_var_screeninfo *ptr = (struct fb_var_screeninfo *) asPtr(p);
    ptr->hsync_len = hsyncLen;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_setVsyncLen
(JNIEnv * env, jobject object, jlong p, jint vsyncLen) {
    struct fb_var_screeninfo *ptr = (struct fb_var_screeninfo *) asPtr(p);
    ptr->vsync_len = vsyncLen;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_setSync
(JNIEnv * env, jobject object, jlong p, jint sync) {
    struct fb_var_screeninfo *ptr = (struct fb_var_screeninfo *) asPtr(p);
    ptr->sync = sync;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_setVmode
(JNIEnv * env, jobject object, jlong p, jint vmode) {
    struct fb_var_screeninfo *ptr = (struct fb_var_screeninfo *) asPtr(p);
    ptr->vmode = vmode;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_setRotate
(JNIEnv * env, jobject object, jlong p, jint rotate) {
    struct fb_var_screeninfo *ptr = (struct fb_var_screeninfo *) asPtr(p);
    ptr->rotate = rotate;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024FbVarScreenInfo_setColorspace
(JNIEnv * env, jobject object, jlong p, jint colorspace) {
    struct fb_var_screeninfo *ptr = (struct fb_var_screeninfo *) asPtr(p);
    ptr->colorspace = colorspace;
}

// EPDSystem.MxcfbUpdateData

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_sizeof
(JNIEnv * env, jobject object) {
    return (jint) sizeof (struct mxcfb_update_data);
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_getUpdateRegionTop
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct mxcfb_update_data *) asPtr(p))->update_region.top;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_getUpdateRegionLeft
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct mxcfb_update_data *) asPtr(p))->update_region.left;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_getUpdateRegionWidth
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct mxcfb_update_data *) asPtr(p))->update_region.width;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_getUpdateRegionHeight
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct mxcfb_update_data *) asPtr(p))->update_region.height;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_getWaveformMode
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct mxcfb_update_data *) asPtr(p))->waveform_mode;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_getUpdateMode
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct mxcfb_update_data *) asPtr(p))->update_mode;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_getUpdateMarker
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct mxcfb_update_data *) asPtr(p))->update_marker;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_getTemp
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct mxcfb_update_data *) asPtr(p))->temp;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_getFlags
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct mxcfb_update_data *) asPtr(p))->flags;
}

jlong JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_getAltBufferDataVirtAddr
(JNIEnv * env, jobject object, jlong p) {
    return asJLong(((struct mxcfb_update_data *) asPtr(p))->alt_buffer_data.virt_addr);
}

jlong JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_getAltBufferDataPhysAddr
(JNIEnv * env, jobject object, jlong p) {
    return (jlong) ((struct mxcfb_update_data *) asPtr(p))->alt_buffer_data.phys_addr;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_getAltBufferDataWidth
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct mxcfb_update_data *) asPtr(p))->alt_buffer_data.width;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_getAltBufferDataHeight
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct mxcfb_update_data *) asPtr(p))->alt_buffer_data.height;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_getAltBufferDataUpdateRegionTop
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct mxcfb_update_data *) asPtr(p))->alt_buffer_data.alt_update_region.top;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_getAltBufferDataUpdateRegionLeft
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct mxcfb_update_data *) asPtr(p))->alt_buffer_data.alt_update_region.left;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_getAltBufferDataUpdateRegionWidth
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct mxcfb_update_data *) asPtr(p))->alt_buffer_data.alt_update_region.width;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_getAltBufferDataUpdateRegionHeight
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct mxcfb_update_data *) asPtr(p))->alt_buffer_data.alt_update_region.height;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_setUpdateRegion
(JNIEnv * env, jobject object, jlong p, jint top, jint left, jint width, jint height) {
    struct mxcfb_update_data *ptr = (struct mxcfb_update_data *) asPtr(p);
    ptr->update_region.top = (__u32) top;
    ptr->update_region.left = (__u32) left;
    ptr->update_region.width = (__u32) width;
    ptr->update_region.height = (__u32) height;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_setWavevformMode
(JNIEnv * env, jobject object, jlong p, jint mode) {
    struct mxcfb_update_data *ptr = (struct mxcfb_update_data *) asPtr(p);
    ptr->waveform_mode = (__u32) mode;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_setUpdateMode
(JNIEnv * env, jobject object, jlong p, jint mode) {
    struct mxcfb_update_data *ptr = (struct mxcfb_update_data *) asPtr(p);
    ptr->update_mode = (__u32) mode;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_setUpdateMarker
(JNIEnv * env, jobject object, jlong p, jint marker) {
    struct mxcfb_update_data *ptr = (struct mxcfb_update_data *) asPtr(p);
    ptr->update_marker = (__u32) marker;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_setTemp
(JNIEnv * env, jobject object, jlong p, jint temp) {
    struct mxcfb_update_data *ptr = (struct mxcfb_update_data *) asPtr(p);
    ptr->temp = (int) temp;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_setFlags
(JNIEnv * env, jobject object, jlong p, jint flags) {
    struct mxcfb_update_data *ptr = (struct mxcfb_update_data *) asPtr(p);
    ptr->flags = (uint) flags;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_setAltBufferData
(JNIEnv * env, jobject object, jlong p, jlong virtAddr, jlong physAddr, jint width, jint height,
        jint updateRegionTop, jint updateRegionLeft, jint updateRegionWidth, jint updateRegionHeight) {
    struct mxcfb_update_data *ptr = (struct mxcfb_update_data *) asPtr(p);
    ptr->alt_buffer_data.virt_addr = asPtr(virtAddr);
    ptr->alt_buffer_data.phys_addr = (__u32) physAddr;
    ptr->alt_buffer_data.width = (__u32) width;
    ptr->alt_buffer_data.height = (__u32) height;
    ptr->alt_buffer_data.alt_update_region.top = (__u32) updateRegionTop;
    ptr->alt_buffer_data.alt_update_region.left = (__u32) updateRegionLeft;
    ptr->alt_buffer_data.alt_update_region.width = (__u32) updateRegionWidth;
    ptr->alt_buffer_data.alt_update_region.height = (__u32) updateRegionHeight;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbUpdateData_print
(JNIEnv * env, jobject object, jlong p) {
    struct mxcfb_update_data *ptr = (struct mxcfb_update_data *) asPtr(p);

    printf("mxcfb_update_data (%u bytes at %p)\n", sizeof *ptr, ptr);
    printf("    update_region.top = %u\n", ptr->update_region.top);
    printf("    update_region.left = %u\n", ptr->update_region.left);
    printf("    update_region.width = %u\n", ptr->update_region.width);
    printf("    update_region.height = %u\n", ptr->update_region.height);

    printf("    waveform_mode = %u\n", ptr->waveform_mode);
    printf("    update_mode = %u\n", ptr->update_mode);
    printf("    update_marker = %u\n", ptr->update_marker);
    printf("    temp = %d\n", ptr->temp);
    printf("    flags = %u\n", ptr->flags);

    printf("    alt_buffer_data.virt_addr = %p\n", ptr->alt_buffer_data.virt_addr);
    printf("    alt_buffer_data.phys_addr = %u\n", ptr->alt_buffer_data.phys_addr);
    printf("    alt_buffer_data.width = %u\n", ptr->alt_buffer_data.width);
    printf("    alt_buffer_data.height = %u\n", ptr->alt_buffer_data.height);
    printf("    alt_buffer_data.alt_update_region.top = %u\n", ptr->alt_buffer_data.alt_update_region.top);
    printf("    alt_buffer_data.alt_update_region.left = %u\n", ptr->alt_buffer_data.alt_update_region.left);
    printf("    alt_buffer_data.alt_update_region.width = %u\n", ptr->alt_buffer_data.alt_update_region.width);
    printf("    alt_buffer_data.alt_update_region.height = %u\n", ptr->alt_buffer_data.alt_update_region.height);
}

// EPDSystem.MxcfbWaveformModes

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbWaveformModes_sizeof
(JNIEnv * env, jobject object) {
    return (jint) sizeof (struct mxcfb_waveform_modes);
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbWaveformModes_getModeInit
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct mxcfb_waveform_modes *) asPtr(p))->mode_init;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbWaveformModes_getModeDu
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct mxcfb_waveform_modes *) asPtr(p))->mode_du;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbWaveformModes_getModeGc4
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct mxcfb_waveform_modes *) asPtr(p))->mode_gc4;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbWaveformModes_getModeGc8
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct mxcfb_waveform_modes *) asPtr(p))->mode_gc8;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbWaveformModes_getModeGc16
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct mxcfb_waveform_modes *) asPtr(p))->mode_gc16;
}

jint JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbWaveformModes_getModeGc32
(JNIEnv * env, jobject object, jlong p) {
    return (jint) ((struct mxcfb_waveform_modes *) asPtr(p))->mode_gc32;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbWaveformModes_setModes
(JNIEnv * env, jobject object, jlong p, jint init, jint du, jint gc4, jint gc8, jint gc16, jint gc32) {
    struct mxcfb_waveform_modes *ptr = (struct mxcfb_waveform_modes *) asPtr(p);
    ptr->mode_init = (int) init;
    ptr->mode_du = (int) du;
    ptr->mode_gc4 = (int) gc4;
    ptr->mode_gc8 = (int) gc8;
    ptr->mode_gc16 = (int) gc16;
    ptr->mode_gc32 = (int) gc32;
}

void JNICALL Java_com_sun_glass_ui_monocle_EPDSystem_00024MxcfbWaveformModes_print
(JNIEnv * env, jobject object, jlong p) {
    struct mxcfb_waveform_modes *ptr = (struct mxcfb_waveform_modes *) asPtr(p);
    printf("mxcfb_waveform_modes (%u bytes at %p)\n", sizeof *ptr, ptr);
    printf("    mode_init = %d\n", ptr->mode_init);
    printf("    mode_du = %d\n", ptr->mode_du);
    printf("    mode_gc4 = %d\n", ptr->mode_gc4);
    printf("    mode_gc8 = %d\n", ptr->mode_gc8);
    printf("    mode_gc16 = %d\n", ptr->mode_gc16);
    printf("    mode_gc32 = %d\n", ptr->mode_gc32);
}
